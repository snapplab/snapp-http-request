const {HttpMethods, Url} = global.SnappFramework;

module.exports = function (self) {
  self.request = this.object;
  self.method = HttpMethods[self.request.method.toLowerCase()];
  self.url = Url(self.request.url);
};
